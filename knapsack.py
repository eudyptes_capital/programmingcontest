#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import string
from pyevolve import G1DList, GSimpleGA, Selectors
from pyevolve import Initializators, Mutators, Consts

class CalcKnapzak:
	"""
	ナップザック問題を解決するクラス
	"""

	def exec_calc(self):
		self.__init_param()
		computation = self.__exec_genetic_algorithm()
		return computation

	def evalution(self, data):
		"""
		評価関数
		"""

		eval_money_rate = 10 # 金額に10倍の評価差をつける
		return abs(1250 - self.__eval_money(data)) * eval_money_rate + self.__eval_sum(data)


	def __eval_money(self, data):
		return data[0] + 5*data[1] + 10*data[2] + 50*data[3] + 100*data[4] + 500*data[5]

	def __eval_sum(self, data):
		return sum(data)

	def __init_param(self):
		"""
		基本パラメータを初期化するメソッド
		"""
		self.genome_num = 6 #遺伝子数 左から[1,5,10,50,100,500]
		self.genome_range={'min':0, 'max':10}
		self.eval_type = 'minimize'; # 評価手法タイプ
		self.generation_num = 3000 # 世代数
		self.crossover_rate = 0.90 # 交差率
		self.mutation_rate = 0.02 # 突然変異率
		self.population_size =50 # 集団数
		self.freq_stats = 100 # 表示間隔

	def __create_genome(self):
		"""
		遺伝子の生成
		"""

		genome = G1DList.G1DList(self.genome_num) # 要素数で遺伝子を初期化
		genome.setParams(rangemin = self.genome_range['min'], rangemax = self.genome_range['max']) # 要素範囲
		genome.initializator.set(Initializators.G1DListInitializatorInteger) # 範囲の間で整数で初期化
		genome.mutator.set(Mutators.G1DListMutatorIntegerGaussian) # 突然変異はガウス整数を
		genome.evaluator.set(self.evalution) # 評価関数をセット

		return genome

	def __create_simple_genome_algorithm_instance(self):
		"""
		遺伝的アルゴリズムインスタンス生成
		"""

		# 遺伝的アルゴリズム ルール設定
		genome = self.__create_genome()
		ga = GSimpleGA.GSimpleGA(genome)
		ga.selector.set(Selectors.GRouletteWheel) # ルーレット選択方式
		# ga.selector.set(Selectors.GRankSelector) # ランキング選択方式
		# ga.terminationCriteria.set(GSimpleGA.FitnessStatsCriteria) # 適合度が安定したら終了する
		ga.setMinimax(Consts.minimaxType[self.eval_type])

		# 遺伝的アルゴリズムのパラメータ設定
		ga.setGenerations(self.generation_num) # 世代数
		ga.setCrossoverRate(self.crossover_rate) # 交差率
		ga.setMutationRate(self.mutation_rate) # 突然変異率
		ga.setPopulationSize(self.population_size) # 集団数
		ga.evolve(freq_stats = self.freq_stats) # 表示間隔

		return ga

	def __exec_genetic_algorithm(self):
		ga = self.__create_simple_genome_algorithm_instance()

		best = ga.bestIndividual() # 最も評価が高い個体を表示
		return best

	def print_eval(self, computation):
		print computation
		print self.__eval_money(computation)
		print self.__eval_sum(computation)


class TargetData:
	'''
	Targetデータ格納クラス
	'''

	# 対象迷路
	def __init__(self):
		self.target = [1,2,3,4,5]

	def get(self):
		return self.target

	def set(self, target):
		self.target = target

if __name__ == "__main__":
	knapzak = CalcKnapzak()
	computation = knapzak.exec_calc()
	knapzak.print_eval(computation)

