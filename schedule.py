#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import string
from pyevolve import G1DList, GSimpleGA, Selectors
from pyevolve import Initializators, Mutators, Consts

class CalcSchedule:
	"""
	スケージュール問題を解決するクラス
	"""

	def __init__(self):
		self.no_select = -1
		self.__init_ga_param()

	def __init_ga_param(self):
		"""
		基本パラメータの初期化
		"""
		self.target = TargetData().get()
		self.lap_weight = 2 # 重複重み
		target_length = len(self.target)
		self.genome_num = 10 # 要素数(遺伝子数）
		self.genome_range={'min':self.no_select - target_length, 'max':target_length - 1}
		self.eval_type = 'maximize'; # 評価手法タイプ('maximize', 'minimize')
		self.generation_num = 10000 # 世代数
		self.crossover_rate = 0.90 # 交差率
		self.mutation_rate = 0.10 # 突然変異率
		self.population_size = 50 # 集団数
		self.freq_stats = 100 # 表示間隔

	def exec_calc(self):
		"""
		計算メイン関数
		"""
		computation = self.__exec_genetic_algorithm()
		result =  self.__generate_result_from_ga_computation(computation)
		return computation, result

	def evalution(self, data):
		"""
		評価関数
		"""
		time_table, lap_num = self.__fill_time_table(data, False)
		eval = len(time_table) - lap_num * self.lap_weight # 評価は、タイムテーブルで埋まった数 - タイムテーブルが重複した数*重み付け
		if (eval < 0):
			eval = 0
		return eval


	def __fill_time_table(self, data, debug):
		"""
		タイムテーブルを埋める関数
		"""
		time_table = []
		lap_num = 0

		# スケージュールが重ならないようにタイムテーブルを埋めていく
		data_length = len(data)
		if (debug):
			print "debug_pekimoche", data_length
		for n in range(data_length):
			if (data[n] <= self.no_select):
				continue # 選択しなかったので負の値=self.no_selectが入る

			# 初めと最後だけ見てすでにタイムテーブルに入っていなければ間を仕事で埋める
			time_start = self.target[data[n]]['start']
			time_end = self.target[data[n]]['end'] + 1
			if (debug):
				print "debug_pekimoche", time_start, time_end

			# スケージュールが埋まっているか
			is_filled = False
			for time in range(time_start, time_end):
				if (time in time_table):
					is_filled = True
					lap_num += 1 # 重なった値があるとまずいのでカウント
					break

			# スケージュールが空いていれば、タイムテーブルを埋める
			if (not is_filled):
				for time in range(time_start, time_end):
					time_table.append(time)

		return time_table, lap_num

	def __generate_result_from_ga_computation(self, computation):
		"""
		計算結果から結果を抽出する
		"""

		computation = list(set(computation)) # 重複削除
		result = []
		for n in computation:
			# 選択しなかった値は弾く
			if (n > self.no_select):
				result.append(n)
		return result

	def __create_genome(self):
		"""
		遺伝子の生成
		"""
		genome = G1DList.G1DList(self.genome_num) # 要素数で遺伝子を初期化
		genome.setParams(rangemin = self.genome_range['min'], rangemax = self.genome_range['max']) # 要素範囲
		genome.initializator.set(Initializators.G1DListInitializatorInteger) # 範囲の間で整数で初期化
		genome.mutator.set(Mutators.G1DListMutatorIntegerGaussian) # 突然変異はガウス整数を
		genome.evaluator.set(self.evalution) # 評価関数をセット
		return genome

	def __create_simple_genome_algorithm_instance(self):
		"""
		遺伝的アルゴリズムインスタンス生成
		"""

		# 遺伝的アルゴリズム ルール設定
		genome = self.__create_genome()
		ga = GSimpleGA.GSimpleGA(genome)
		ga.selector.set(Selectors.GRouletteWheel) # ルーレットセレクト方式を選択(GRouletteWheel, GRankSelector)
		# ga.terminationCriteria.set(GSimpleGA.FitnessStatsCriteria) # 適合度が安定したら終了する
		ga.setMinimax(Consts.minimaxType[self.eval_type])

		# 遺伝的アルゴリズムのパラメータ設定
		ga.setGenerations(self.generation_num) # 世代数
		ga.setCrossoverRate(self.crossover_rate) # 交差率
		ga.setMutationRate(self.mutation_rate) # 突然変異率
		ga.setPopulationSize(self.population_size) # 集団数
		ga.evolve(freq_stats = self.freq_stats) # 表示間隔
		return ga

	def __exec_genetic_algorithm(self):
		"""
		遺伝的アルゴリズム実行
		"""
		ga = self.__create_simple_genome_algorithm_instance()

		best = ga.bestIndividual() # 最も評価が高い個体を計算
		return best

	def print_eval(self, computation, result):
		"""
		評価をターミナルに出力
		"""
		time_table, lap_num = self.__fill_time_table(computation, True)
		time_table.sort() # タイムテーブル結果
		print computation # ga計算結果
		for i in result:
			print self.target[i]['start'], self.target[i]['end'] # 問題結果
		print "computation:"
		print self.evalution(computation), lap_num # 評価結果 重複数
		print time_table # タイムテーブル結果


class TargetData:
	'''
	Targetデータ格納クラス
	'''

	# 対象迷路
	def __init__(self):
		self.num = 100
		self.start = [19, 34, 5, 39, 6, 17, 43, 13, 33, 6, 25, 10, 3, 32, 30, 25, 23, 20, 38, 31, 39, 10, 34, 47, 49, 37, 19, 28, 20, 17, 22, 20, 40, 0, 20, 6, 49, 46, 0, 23, 41, 13, 0, 14, 49, 41, 36, 46, 3, 28, 27, 29, 22, 37, 47, 16, 31, 14, 30, 27, 35, 49, 0, 44, 40, 49, 41, 1, 6, 39, 37, 4, 39, 10, 31, 3, 37, 18, 47, 13, 47, 33, 4, 0, 34, 36, 42, 17, 1, 38, 30, 35, 8, 5, 1, 4, 7, 17, 26, 35]
		self.end = [34, 67, 39, 62, 32, 49, 50, 13, 43, 23, 25, 16, 48, 51, 37, 68, 46, 42, 51, 49, 76, 14, 79, 48, 87, 73, 54, 34, 65, 23, 44, 51, 49, 3, 41, 7, 86, 72, 25, 30, 63, 13, 39, 27, 88, 89, 57, 46, 31, 58, 61, 75, 34, 85, 93, 49, 54, 61, 56, 72, 54, 51, 6, 46, 50, 59, 73, 33, 31, 86, 61, 37, 81, 16, 64, 39, 85, 42, 57, 40, 62, 69, 28, 30, 38, 57, 66, 47, 36, 38, 35, 45, 30, 46, 45, 37, 16, 26, 42, 43]


	def get(self):
		"""
		ターゲットデータを返す
		['start':1, 'end':4]
		"""

		list =[]
		for i in range(self.num):
			list.append({'start':self.start[i], 'end':self.end[i]})
		return list

if __name__ == "__main__":

	schedule = CalcSchedule()
	computation, result = schedule.exec_calc() # 計算実行
	schedule.print_eval(computation, result)
