import java.util.HashMap;

public class LongestCommonSubsequence {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ
		String target1 = "kgsbyshnthednsehtrgabjmnhnkafwwrnpsuxdbrmfggsgjdrfbcpjyshxdtirzzpytngmjwmfjtduftiwufmxmduxehmtkbureziurphzjzbwwayxuwaandywbneinkiyurhbtkmsbkmmnbjiriupxchtpbsefrnwbhhtxndbdpgdhkjmrtkafxaxziajwweczbsarjuukemchsrbusjnexwwrumsferygnuhkyiadrdrrzxzusxwfcazgmejintyjesfdbdewekepezmmtfwbuynwcustjmzwjxgcbcdxxrrkfpjygidaebatjnweyhryejgzmdmjhdpziucxdtxgcmjjdsjdkmhsdkperpfchcbsszimehtzacmdjpzusnunzcnmrejkjnhuhgmdwpcdnfgdzszrjyjibfkgagmadzkfhzmwesrkgcwruaynadizrngpdimbxhtkaiezhrkgxhdtdmjkptzprsxkbtuzfkpumxenwkminrdeaeftheamxcenzasjkabypgkgrytnyszeunszkcihuuyfcfacdxaepjknekfjeigcnhngufuxbtawtuyhrbehnbhxyfjgrgwywhzsgnptcmtmfkawjtnrybmuwgydrdhbjkgbufsaaeniyywyukmkwsbttprusuejceaupbsyywpwpehsduzngmxrepwabhpdgybhxfbyywxspzznsjfpbetgkfpyweyumrjijukhxbdajsnkpdwjdtjkbtbmazbkyzxtwmiiedpabdacxjykhaeeatudfcucngxygmkzmcatsxsnghmatsbfhiudruxnswbxwzkcyeunhwkkffzscxyzriytgcwmxpjtuxcikgrrtfxidrssuxipdkpxuaymgtzzfutummxgbmkesszcgkunbsbffertgtbxfnaeifkwfkksfupfyxweaufktscyxjeagfrdnctupkwmtemypxgabprdxtfzzkhfntatsbyxm";
		String target2 = "righknxxdtrbebwwrsbkuhtsxfhiuepnneyyjzgwdmnynxgjjadjabaukurrzsnditncgygexneyxwnpubfhgikdkmbjttagrcmzkgxjuxexbhhyjgashcpjrjdgauestafscdtxhywpaekecyjyjhihajypisaxbahkjyxsnxrphihmdcdauyyfapnrdyuhmnkayrpfapxbzbsbxumrfszywjspzgngwiiixwagkshppdbsuzpisrhtfzehcjbtxrfpmxssexititfaiytfahwizkyeppyiywkpgjxrziwwhnbncpcrrsdkadrxbjyimegyjdwptcpwpuscnkanrhreuywwapkjdppnajuswiupeffnzjasmjtjrhuxcysgmisyfmeaspseyxgpzsrpfwsfieynbbgxfpeucsfyunhfdkhyspkjprjppmrsftxtazyagyrujrrkmainaxefbjmmhcbhztkcnizypyfmymstuscfafepipzrwpbdicmhmeizisjctxtrhetifxmmrpxzccbhkfkutzsbuxgwzwbyycezeeykbaccmcjucgmjbxrjwiyuuynhfyrufjhxencubdgsnkriszpbdjefbbrmkgxuaeputmetxuetyksparnkxizjfcfbtgswjmpaddiufgdkbxdpyergcjkahibtnpjmsmmemdttssmanwirwneggkchzrbzjgdheyppfdnacdutxraszdhsjfurnbycxjgyprasdrdzmjhmufykgwdnbzjzuxxnezrrmcbpjubsucdebgrhaajzigwgcwpgfscidiygsawskwetepakrybnprdnickbgpuijhhkinakeygrnjfhfxnytrfjummknwgthuxirhtzgmcrmzudpyryzncytahkzigpecgedbzuwyycyuxdtdejphyyxdguuumybbjpdcmgharpwpczxznjmhxrnesnbrggfnbscmgnjteygpizbfsbnrjh";

		int[][] lcs = calculate_lcs(target1, target2);
		String result = generate_lcs_string("", lcs, target1, target1.length()-1, target2.length()-1);

		// 結果
		System.out.println(result);
		System.out.println(result.length());
	}

	/**
	 * lcs結果から、最長経路の文字列を返す
	 * @param target1 対象文字列１
	 * @param target2 対象文字列２
	 * @return int[][] LCS文字列長配列
	 */
	private static int[][] calculate_lcs(String target1, String target2) {
		HashMap<String, Integer> neighborhood;
		int[][] lcs = new int[target1.length()][target2.length()];

		// target1 と target2 を軸とした二次元座標に置き換え、左上の文字列から順に参照していく
		for (int i=0; i<target1.length(); i++) {
			for (int j=0; j<target2.length(); j++) {
				neighborhood = get_neighborhood(lcs, i, j); // 付近の値を取得

				// 最長経路を計算する
				lcs[i][j] = target1.charAt(i) == target2.charAt(j) ?
					neighborhood.get("left_side_up") + 1 : Math.max(neighborhood.get("left"), neighborhood.get("up"));
			}
		}
		return lcs;
	}

	/**
	 * lcs結果から、最長経路の文字列を返す
	 * @param result
	 * @param lcs
	 * @param target1
	 * @param i
	 * @param j
	 * @return HashMap<String, Integer>
	 */
	private static String generate_lcs_string(String result, int[][] lcs, String target1, int i, int j) {
		if (i<0 || j<0) return result;
		HashMap<String, Integer> neighborhood = get_neighborhood(lcs, i, j); // 付近の値を取得
		if (lcs[i][j] -1 == neighborhood.get("left_side_up").intValue()) {
			result = generate_lcs_string(result, lcs, target1, i-1, j-1);
			result += target1.charAt(i);
		} else if (lcs[i][j] == neighborhood.get("up").intValue()) {
			result = generate_lcs_string(result, lcs, target1, i-1, j);
		} else if (lcs[i][j] == neighborhood.get("left").intValue()) {
			result = generate_lcs_string(result, lcs, target1, i, j-1);
		}
		return result;
	}

	/**
	 * 対象ポイントの周囲の値を返す
	 * @param lcs
	 * @param i
	 * @param j
	 * @return HashMap<String, Integer>
	 */
	private static HashMap<String, Integer> get_neighborhood(int[][] lcs, int i, int j) {
		HashMap<String, Integer> neighborhood = new HashMap<String, Integer>();
		neighborhood.put("left_side_up", (i-1>=0 && j-1>=0) ? lcs[i-1][j-1] : 0);
		neighborhood.put("up", i-1>=0 ? lcs[i-1][j] : 0);
		neighborhood.put("left", j-1>=0 ? lcs[i][j-1] : 0);
		return neighborhood;
	}

}
