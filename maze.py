#!/usr/bin/env python
# -*- coding: utf-8 -*-

import copy
import string
from pyevolve import G1DList, GSimpleGA, Selectors
from pyevolve import Initializators, Mutators, Consts

class Maze:
	"""
	迷路を探索するクラス
	左上が原点(0,0)で右に行くほどプラス、下に行くほどプラスの座標系
	"""

	def __init__(self):
		# 定義
		self.maze_terms = {'start':'S', 'block':'#', 'pass':'.', 'goal':'G',}
		self.direction = {'right':1, 'left':2, 'up':3, 'down':4}
		self.max = 1000
		# self.max = 100000000

	def evalution(self, target, course_list):
		'''
		evalution関数
		迷路とそのルートに対するスコアを計算し、その評価を返す
		@param target 迷路
		@param course_list コースリスト
		'''

		# ルート初期化
		self.root_list = []

		# 縦幅の取得
		self.maze = target
		self.width = len(self.maze[0])
		self.height = len(self.maze)

		current_position = self.__get_start_point() # start地点

		for course in course_list:
			self.__move(current_position, course)

		# スコア計算
		len_root_list = len(self.root_list)
		if (self.maze[current_position['y']][current_position['x']] == self.maze_terms['goal']):
			score = self.max + len_root_list
			# score = self.max - len_root_list
		else:
			score = len_root_list

		if (score<0):
			score = 0

		if (self.validate_dead_end(current_position)):
			self.maze = self.__maze_substitution(current_position, '@') # 壁にする

		return score, self.maze

	def __get_start_point(self):
		"""
		start地点を取得する
		return 座標系 ['x', 'y']
		"""

		for y, maze_raw in enumerate(self.maze):
			for x, value in enumerate(maze_raw):
				if (value == self.maze_terms['start']):
					# start地点ヒットしたので、位置を返す
					return {'x':x, 'y':y}

	def __move(self, current_position, course):
		"""
		@param current_position: 現在座標
		@param course 移動方向
		"""

		# 移動する
		position = copy.copy(current_position)
		if (course == self.direction['right']):
			position['x'] += 1
		elif (course == self.direction['left']):
			position['x'] -= 1
		elif (course == self.direction['up']):
			position['y'] -= 1
		elif (course == self.direction['down']):
			position['y'] += 1

		if (self.__is_moved(position)):
			# 移動可能なので動く
			current_position['x'] = position['x']
			current_position['y'] = position['y']
			self.root_list.append([position['x'], position['y']]) # 通った道リストに追加

	def __maze_substitution(self, position, maze_term):
		'''
		迷路文字列に指定した文字を代入する関数
		@param position 判定座標
		@param maze_term 迷路用文字
		'''

		new_maze = []
		for y, maze_raw in enumerate(self.maze):
			row = ''
			for x, value in enumerate(maze_raw):
				if (position == {'x':x, 'y':y} and value != self.maze_terms['start'] and value != self.maze_terms['goal']):
					row += maze_term
				else:
					row += value

			new_maze.append(row)

		return new_maze


	def __is_moved(self, position, validate_before_root = True):
		'''
		移動可能かの判定関数
		@param position 判定座標
		@poram validate_before_root 以前に通ったルートを判定に含めるか
		'''

		# 迷路の範囲内か
		in_range = (0 <= position['x'] and position['x'] < self.width and 0 <= position['y'] and position['y'] < self.height)

		# 以前に通過したルートか
		before_moving_root = True
		if (validate_before_root):
			before_moving_root = [position['x'], position['y']] not in self.root_list

		if (in_range and before_moving_root):
			if (self.maze[position['y']][position['x']] == self.maze_terms['pass']
				or self.maze[position['y']][position['x']] == self.maze_terms['start']
				or self.maze[position['y']][position['x']] == self.maze_terms['goal']):
				return True
		return False

	def validate_dead_end(self, current_position):
		'''
		行き止まりかの判定関数
		'''

		result = 0
		result += self.__is_moved({'x':current_position['x'] + 1, 'y':current_position['y']}, False)
		result += self.__is_moved({'x':current_position['x'] - 1, 'y':current_position['y']}, False)
		result += self.__is_moved({'x':current_position['x']    , 'y':current_position['y'] + 1}, False)
		result += self.__is_moved({'x':current_position['x']    , 'y':current_position['y'] - 1}, False)

		if (result <= 1 and self.maze[current_position['y']][current_position['x']] != self.maze_terms['goal']):
			return True
		else:
			return False

	def go_root_debug(self, target, course_list):
		'''
		デバッグ用関数
		@param target 迷路文字列
		@poram course_list コースリスト
		'''

		# ルート初期化
		self.root_list = []

		# 縦幅の取得
		self.maze = target
		self.width = len(self.maze[0])
		self.height = len(self.maze)

		current_position = self.__get_start_point() # start地点
		for course in course_list:
			self.__move(current_position, course)
			# print current_position['x'], current_position['y'], self.maze[current_position['y']][current_position['x']]
			self.maze = self.__maze_substitution(current_position, 'O') # 通過した印をつける

		return self.maze


class TargetData:
	'''
	Targetデータ格納クラス
	'''

	# 対象迷路
	def __init__(self):
		self.target ="""\
.S.#.##.####.##....#..#..#........................
.#.#............##.#.###.#.###.##################.
##.#.#############.#..#..#...#.#.....#......#.#.#.
...#...#........##.#....####.#.##.#..#.###......#.
.#.#.#....#.######.#.#.......#.#..#........###..#.
.######.###.#...##.#.#.#.##..#.#.#########.#.##.#.
..........#.#.#.##.#.#######.#.#.#...#...#.#.#..#.
.#.########.#.#.##.#....##.#.#.#.#.#.#...#.#.#..#.
.#.##.#.#.#.#.#.#..#.##......#.#.#.#...#.#.#.#..#.
.#....#.#.#.#.#.#.##.#########.#.#######.#.#.#.##.
#######.#...#.#.#......#.....#.#.........#.#.#..#.
..#...#.#.#.#.#.#..#####.#.#.#.###########.#.#..#.
......#.#.#...#.#......#..#..#.....#.......#.#..#.
#####.#.#.#####.#####..#.....#..##...#.#...#.##.#.
..#...#.#.#.###.#.########.#########.#.#...#.#..#.
..#.#...#.#.......#..#.................###.#.#..#.
....#.....#.#####.#.##.#################...#....#.
.##########.#.#.#.#..#..#..#.........#.#..#######.
.#..#.....#.....#.#.###.#.....#####....#..#.......
...##.#.#....##.#.#..#..#.##########.#.##.#.######
.#..#.#.#.#######.##.#.##.#..........#.##.#...####
.##.#####.#.......##....#.#.########.#.##.#.#.#...
#####.....#############.###.#.....##.#.##.#.#.#.#.
.......#..............#...#.#####.##.#.##.#.#.#.#.
####.####.............###.#.#####.##.#.##.#.#.#.#.
...#.#..................#.#.......##.#.##.#.#.#.#.
.#.#....###..###........#.##########.#.##.#.#.#.#.
.#.#.#.#.....#..........#............#.##.#.#.#.#.
.#.#...###...#.###......#.############.##.#.###.#.
.#.#.#....#..#..#.......#.#.#############.#..##.#.
.#.#......#..#..#.......#.#.#.............##.##.#.
.#.#.#.###...####.......#.#.#.##############.##.#.
.#.#....................#.#.#.#...........##.##.#.
.#.#.#......###..###....#.#.#.#.############.##.#.
.#.#.......#.....#......#.#.#.#.############.##.#.
.#.#.#.....###...#.###..#.#.#.#.#.........##.##.#.
.#.#..........#..#..#...#.#.#.#.#.#######.##.##.#.
.#.#.#........#..#..#...#.#.#.#.#.#.#G#.#.##.##.#.
.#.#.#.....###...####...#.#.#.#.#.#.#.#.#.##.##.#.
.#.#.#..................#.#.#.#.#.#.#.#.#.##.##.#.
.#.#.#...###..###.......#.#.#.#.#.#.......##.##.#.
.#.#.#..#.....#.....###.#.#.#.#.#.##########.##.#.
.#.#.#..###...#.###.#.#.#.#.#.#.#............##.#.
.#.#.#.....#..#..#..#.#.#.#.#.#.###############.#.
.#.#.#.....#..#..#..#.#.#.#.#.#.###############.#.
.#.#.#..###...####..#.#.#.#.#.#.................#.
.#.#.#..............#.#.#.#.#.###################.
.#..................#...#.#.#.....................
.########################.#.#####################.
..........................#.......................
""".split()

	def get(self):
		return self.target

	def set(self, target):
		self.target = target


maze = Maze()
target_data = TargetData()

def eval_func(course_list):
	score, updating_target = maze.evalution(target_data.get(), course_list)
	target_data.set(updating_target)
	return score

if __name__ == "__main__":

	genome = G1DList.G1DList(5000) # 要素数
	genome.setParams(rangemin=1, rangemax=4) # 1~4の範囲

	# 範囲の間で整数で初期化
	genome.initializator.set(Initializators.G1DListInitializatorInteger)

	# 突然変異はガウス整数を
	genome.mutator.set(Mutators.G1DListMutatorIntegerGaussian)

	# 評価関数をセット
	genome.evaluator.set(eval_func)

	# 遺伝的アルゴリズム ルール設定
	ga = GSimpleGA.GSimpleGA(genome)
	ga.selector.set(Selectors.GRouletteWheel) # ルーレット方式(迷路は初期収束率を高めた方が探索しやすいという事が分かったので、ランキング方式はやめた)
	ga.terminationCriteria.set(GSimpleGA.FitnessStatsCriteria) # 適合度が安定したら終了する
	ga.setMinimax(Consts.minimaxType["maximize"]) # 評価関数を最大化する

	# 遺伝的アルゴリズムのパラメータ設定
	ga.setGenerations(5000) # 世代数
	ga.setCrossoverRate(0.95) # 交差率 95%
	ga.setMutationRate(0.1) # 突然変異率 1%
	ga.setPopulationSize(30) # 集団の数
	ga.evolve(freq_stats=10) # 表示世代間隔
	best = ga.bestIndividual() # 最も評価が高い個体を表示

	goal_root = []
	for num in best:
		goal_root.append(num)

	result_maze = maze.go_root_debug(target_data.get(), goal_root)
	print '\n'.join(result_maze)
