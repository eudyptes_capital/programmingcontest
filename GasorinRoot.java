
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TreeMap;


/**
 * Valueでソートできるようにする比較クラス
 * @author pekimoche
 *
 */
class ValueComparator implements Comparator<GasorinStand> {

	public int compare(GasorinStand i1, GasorinStand i2) {
		return i2.getValue() - i1.getValue();
	}
}

class GasorinStand {
	Integer key, value;

	public GasorinStand(Integer key, Integer value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return this.key;
	}

	public Integer getValue() {
		return this.value;
	}
}

public class GasorinRoot {

	static int goal_distance = 5000; // 距離
	static int primary_gasoline = 100; // 初めにもってるガソリン
	static int[] stand_point = {1, 76, 138, 155, 243, 260, 335, 435, 498, 536, 564, 594, 602, 636, 695, 744, 750, 838, 869, 966, 1053, 1072, 1108, 1184, 1225, 1271, 1342, 1376, 1391, 1432, 1487, 1541, 1635, 1697, 1755, 1794, 1799, 1812, 1822, 1836, 1860, 1951, 1964, 1997, 2015, 2056, 2057, 2123, 2132, 2187, 2213, 2284, 2373, 2412, 2451, 2530, 2610, 2657, 2734, 2760, 2856, 2912, 3003, 3050, 3070, 3098, 3148, 3223, 3302, 3400, 3432, 3510, 3586, 3604, 3607, 3622, 3713, 3754, 3807, 3848, 3920, 4012, 4085, 4091, 4159, 4227, 4260, 4354, 4360, 4388, 4406, 4426, 4441, 4535, 4623, 4687, 4760, 4824, 4854, 4880};
	static int[] stand_volume = {70, 31, 45, 45, 48, 61, 87, 52, 89, 26, 2, 51, 24, 91, 2, 11, 46, 82, 78, 26, 85, 81, 100, 64, 70, 19, 71, 8, 52, 87, 36, 73, 38, 63, 55, 87, 52, 91, 25, 58, 10, 47, 9, 21, 81, 27, 56, 58, 70, 74, 42, 85, 58, 85, 99, 79, 4, 85, 68, 71, 11, 60, 40, 53, 49, 4, 37, 73, 24, 28, 95, 60, 67, 81, 31, 9, 39, 81, 91, 74, 39, 42, 81, 73, 100, 37, 16, 53, 98, 17, 52, 29, 75, 20, 67, 62, 26, 11, 29, 71};
	static int stand_num = 100;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int reaching_distance = primary_gasoline;
		int before_reaching = 0;
		int i = 0;
		TreeMap<Integer, Integer> used_gasoline_stand = new TreeMap<Integer, Integer>();
		GasorinStand max_volume_stand;
		Queue<GasorinStand> available_gasoline_stand = new PriorityQueue<GasorinStand>(1, new ValueComparator());

		// 行ける距離がゴールを超えたら終了
		while (reaching_distance <= goal_distance) {
			if (before_reaching < stand_num-1) {
				// 行ける距離を算出したら、その間に行けるガソリンスタンドを全てピックアップして、S1に保存する
				for (i=before_reaching; stand_point[i]<=reaching_distance; i++) {
					available_gasoline_stand.add(new GasorinStand(stand_point[i], stand_volume[i]));
					if (i == stand_num-1) break;
				}
				before_reaching = i; // ここまでガソリンスタンドを検出した
			}

			// メモリストから、最大のガソリン容量を持つガソリンスタンドをピックアップして、決定リストに放り込む
			max_volume_stand = (GasorinStand) available_gasoline_stand.poll(); // point => value
			used_gasoline_stand.put(max_volume_stand.getKey(), max_volume_stand.getValue()); // point => volume

			// 次の行ける最大の距離
			reaching_distance += max_volume_stand.getValue();
			if (used_gasoline_stand.size() == stand_num) break; // 到達できなかった
		}

		// 計算完了
		System.out.println(reaching_distance);
		System.out.println(used_gasoline_stand);
		System.out.println(used_gasoline_stand.size());
	}
}
