#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import copy
from datetime import datetime
import math
import random

class PartialSumProgrem:
	"""
	部分和問題を解決するクラス
	"""

	def __init__(self, sum_goal):
		self.sum_goal = sum_goal
		self.goal_list = []
		self.research_count = 0

	def main(self, num_list):
		first_time = datetime.now()
		num_list = self.__sorted(num_list)
		print "sort_list:", num_list
		self.__search(num_list, []) # 探索
		end_time = datetime.now()

		sum_goal_list = sum(self.goal_list)
		if (sum_goal_list == self.sum_goal):
			print "research_count=", self.research_count, "goal=", self.goal_list, " sum=", self.sum_goal
		else:
			print "research_count=", self.research_count, "sum=", self.sum_goal, " is not found !!"
		print end_time - first_time

	def __sorted(self, old_list):
		"""
		目標値/数を基準値として、その数値から最も離れた数を先頭に持ってくるようにソートする
		評価関数との相性が良く、明らかに除外候補となるものは初めに削られていき、候補が自動的に削減される
		"""
		ideal_num =  self.sum_goal / len(old_list)
		ideal_diff_list = []
		for key, num in enumerate(old_list):
			ideal_diff_list.append({'key':key, 'num':num, 'diff':math.fabs(num - ideal_num)})

		new_list = []
		for date in sorted(ideal_diff_list, key = lambda x:x['diff'], reverse = True):
			new_list.append(date['num'])
		return new_list

	def __search(self, num_list, selected_list):
		"""
		探索メイン関数
		選択リスト、該当候補リストを元に、探索していく。
		"""
		remain_list = copy.copy(num_list)
		before_num = sys.maxint
		for num in num_list:
			if (num == before_num): continue # 前に同値を調べたので必要なし
			else: before_num = num

			self.__move_target_to_selected_list(remain_list, selected_list, num)
			evalution = self.__evalution_and_update(remain_list, selected_list)
			# print "select", selected_list
			if (evalution < 0):
				# この先を探索しても希望はないので存在を抹消する
				selected_list.remove(num)
				before_not_num = num
				continue
			elif (evalution == 0):
				# この選択リストが結果
				self.goal_list = selected_list
				return
			else:
				# 探索価値あり
				self.__search(remain_list, selected_list) # 残りのリストを漁る
				if (self.goal_list): return # 残りを漁ったら見つかった

			# この値は全探索して見つからなかったので、選択候補から存在を抹消する
			selected_list.remove(num)
			before_not_num = num

	def __move_target_to_selected_list(self, num_list, selected_list, num):
		"""
		値をセレクトする関数
		"""
		self.research_count += 1
		num_list.remove(num)
		selected_list.append(num)

	def __evalution_and_update(self, num_list, selected_list):
		"""
		探索するかの価値を判定する評価関数
		"""
		sum_now = sum(selected_list)
		sum_list = []
		if (sum_now == self.sum_goal):
			return 0 # 探索完了
		else:
			# 終末結果判定
			# 現在の値 < 目標値 <= 残りリストのプラスの数値を全部足した値
			# 残りリストのマイナスの数値を全部足した値 <= 目標値 < 現在の値
			# なら探索する価値あり
			if (sum_now < self.sum_goal):
				for num in num_list:
					if (num > 0): sum_list.append(num)
				sum_all = sum(sum_list) + sum_now
				if (self.sum_goal < sum_all): return 1 # 探索価値あり
			elif (sum_now > self.sum_goal):
				for num in num_list:
					if (num < 0): sum_list.append(num)
				sum_all = sum(sum_list) + sum_now
				if (self.sum_goal > sum_all): return 1 # 探索価値あり

			if (sum_all == self.sum_goal):
				selected_list.extend(sum_list) # 評価してる間に見つけてしまったので即座に更新
				return 0 # 探索完了

		return -1 # 探索価値無し

if __name__ == "__main__":
	num_list = [1, 10, 49, 3, 8, 13, 7, 23, 60, -500, 42, 599, 45, -23, 1, 10, 49, 3, 8, 13]

	# 課題
	# partial_sum_progrem = PartialSumProgrem(444)
	# partial_sum_progrem.main(num_list)

	num_list =[]
	n = 1500
	for i in range(0, n):
		num_list.append(random.randint(-n,n))

	# 課題
	partial_sum_progrem = PartialSumProgrem(random.randint(-n,n))
	partial_sum_progrem.main(num_list)



